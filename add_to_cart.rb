# add product to cart and check product is added to cart.
def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @base_url = "http://origin-www.stest.in/#/"
  @driver.navigate.to @base_url
  @driver.manage.timeouts.implicit_wait = 10
  @driver.manage.window.maximize
end   

def clear_cart
  @driver.find_element(:xpath, "//ul[@id='mini-cart-wrap']/li/div[1]/a[1]").click
  begin
    a = @driver.find_element(:xpath, "//section[@id='cart-container']/div[1]/p").text
    p "Your cart is empty"
  rescue
    p "Your cart is not empty"
    @driver.find_element(:xpath, "//section[@id='cart-container']/div[1]/div[3]/div[1]/a").click   #clear_cart
  end
end

def login
  loggedin_name = []
  p "login"
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='account-wrap']/div/div[1]/a").click 
  sleep(10)
  @driver.find_element(:xpath, "//p[@class='margin-zero']/input[@id='login_email']").send_keys("email_id")
  sleep(10)
  @driver.find_element(:xpath, "//p[@class='margin-zero']/input[@id='login_password']").send_keys("login_password")
  sleep(10)
  @driver.find_element(:xpath, "//form[@id='login-form']/p[5]").click
  sleep(10)
  begin
    p "verifying user is loggedin or not"
    loggedin_name = @driver.find_element(:css, "#account-member-name").text
    sleep(2)
    [loggedin_name].include? "Hello, name"
    p "User is logged into styletag: PASS"
  rescue
    p "User is not logged into styletag: FAIL "
  end
end

def register
  @driver.find_element(:xpath, "//div[@id='account-wrap']/div/div[3]/a").click 
  sleep(10) 
  @driver.find_element(:xpath, "//input[@id='register_name']").clear
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='register_name']").send_keys("name")
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='register_email']").clear
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='register_email']").send_keys("email_id")
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='register_password']").clear
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='register_password']").send_keys("password")
  sleep(10)
  @driver.find_element(:xpath, "//form[@id='registration-form']/p[10]/input[@class='btn']").click
  sleep(10)
end 

def sleepwait
  sleep 20
end

def c1  
  sleepwait
  @driver.find_element(:xpath, "//a[@id='lifestyle']").click
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='sales-container']/div/ul[@class='height-auto'] /li[1][@class='bit-sale-3 sale-wrap ng-scope'] /div[@class='sale-text-wrap height-auto']/div[@class='sale-text-content pull-left']/p[@class='sale-name text-left']/a").click
  sleep(10)
  # hover = @driver.find_element(:css, "#product-container img")
  #  @driver.action.move_to(hover).perform
  #  @driver.find_element(:css, ".products-fullview").click
  @driver.find_element(:xpath, "//div[@id='product-container']/div[@class='ng-isolate-scope']/ul/li[3]/div[@class='products-wrapper']/div[@class='product-Info']/span[@class='product-description text-left']/a").click
  sleep(10)
  begin
    sleep(10)
    @driver.find_element(:xpath, "//ol[@id='sizes']/li[1]/div/label").click  
    sleep(10)
    @driver.find_element(:xpath, "//input[@id='add-to-cart-button']").click
    sleep(10)
  rescue Exception => e
    @driver.find_element(:xpath, "//input[@id='add-to-cart-button']").click
    sleep(10)
  end
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
end 

def cart
  sleepwait
  @driver.find_element(:xpath, "//ul[@id='mini-cart-wrap']/li[@class='mini-cart-inner']/div[@class='minicart-inner-div']/a[@class='col-dark-grey mini-cart-text']").click
  sleep(10)
  p a = @driver.find_element(:xpath, "//div[@id='cart_product_1']/div[3]").text
  sleep(10)
  if a.include? ("Remove")
    p "Product is added to cart: PASS"
  else
    p "Product not added to cart: FAIL"
  end
end

def all
  styletag
  login
  clear_cart
  # register
  c1
  cart
end
all
