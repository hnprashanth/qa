#checkout
require './utils'
include Utils

@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

#wait = Selenium::WebDriver::Wait.new(:timeout => 10) 
#wait.until { driver.title.downcase.start_with? "cheese!" }
 

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @dev_url
  @driver.manage.window.maximize
  # browser = Capybara.current_session.driver.browser
  # browser.manage.delete_all_cookies
  # # @driver.manage.deleteAllCookies
end 

def shortwait
  sleep 5
end

def longwait
  sleep 10
end

def login
  p "login"
  wait_for_spinner
  @driver.find_element(:css, "a.login-link").click
  wait_for_spinner

  @driver.find_element(:css, "#login_email").send_keys("ram@gmail.com")
  @driver.find_element(:css, "#login_password").send_keys("ramramram")
  @driver.find_element(:css, "#login-btn").click
  wait_for_spinner
  unless @driver.find_element(:css, "#account-member-name").displayed?
    raise "User Login Failed"
  else
    puts "Successfully Logged in"
    @driver.save_screenshot("Success-login.png")
  end
end

def register
  p "register"
  wait_for_spinner

  @driver.find_element(:css, "a.register-link").click
  wait_for_spinner

  @driver.find_element(:css, "#register_name").clear
  @driver.find_element(:css, "#register_name").send_keys "AmazingTester"
  @driver.find_element(:css, "#register_email").clear
  number = rand(10...9999)
  @driver.find_element(:css, "#register_email").send_keys "test#{number}@example.net"
  @driver.find_element(:css, "#register_password").clear
  @driver.find_element(:css, "#register_password").send_keys "testing123"
  @driver.action.send_keys(:enter).perform
  wait_for_spinner

  unless @driver.find_element(:css, "#account-member-name").displayed?
    raise "User Registration Failed"
  else
    puts "Successfully Registered"
    @driver.save_screenshot("Success-Register.png")
  end
end

def c1
  # variables for finding classes
  c1_container = @driver.find_element(:css, "a#arts--entertainment")
  p "click c1..."
  c1_container.click
  wait_for_spinner

  first_sale = @driver.find_element(:css, "#sales-container > div > ul > li:nth-child(1) div.double-arrow > a")
  p "click first sale..."
  first_sale.click
  wait_for_spinner

  first_product = @driver.find_element(:css, "#product-container > div > ul > li.product-wrap:nth-child(1) > div.products-wrapper .go-to-product")
  second_product = @driver.find_element(:css, "#product-container > div > ul > li.product-wrap:nth-child(2) > div.products-wrapper .go-to-product")
  p "click first product ... "
  first_product.click
  wait_for_spinner

  begin
    p "select size add to cart ... "
    @driver.find_element(:css, ".in-stock:nth-child(1) div").click
    @driver.find_element(:css, "#add-to-cart-button").click
  rescue Exception => e
    p "no size found. add to cart ... "
    @driver.find_element(:css, "#add-to-cart-button").click
  end
  wait_for_spinner
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
end 

def cart
  p "cart"

  @driver.find_element(:css, ".mini-cart-text").click
  wait_for_spinner

  a = @driver.find_element(:css, "#cart_product_1 > div:nth-child(3) > a").text
  wait_for_spinner

  if a.include? ("Remove")
    p "Product is added to cart: PASS"
  else
    p "Product not added to cart: FAIL"
  end
  wait_for_spinner

  @driver.find_element(:css, "#cart-checkout-btn").click
  wait_for_spinner
  p "Proceed to checkoout ... "
end

def guestLogin
  p "guest login"  
  @driver.find_element(:css, "#checkout-login_email").send_keys("testexp123@styletag.com")
  wait_for_spinner

  @driver.find_element(:css, "#checkout-login_false").click
  wait_for_spinner

  @driver.find_element(:css, "#continue-guest-btn").click
  wait_for_spinner

  @driver.find_element(:css, "#open-two").click
  wait_for_spinner
end

def continue_login
  p "continue as logged in"
  wait_for_spinner

  @driver.find_element(:css, "#open-two").click
  wait_for_spinner
end

def addAddress
  p "add new address"
  # click add new address
  @driver.find_element(:css, ".add-address-block > a#add-address-checkout").click
  wait_for_spinner

  @driver.find_element(:css, "#address_pincode").send_keys("560054")
  wait_for_spinner

  @driver.find_element(:css, "#firstname").send_keys("Test")
  @driver.find_element(:css, "#address-phone").send_keys("9986255327")
  @driver.find_element(:css, "#address_address1").send_keys("#20, Vasanth Nagar, Bangalore")
  @driver.find_element(:css, "#address_landmark").send_keys("Murthy medicals")
  @driver.find_element(:css, "#create-btn").click
  wait_for_spinner
end

def selectAddress
  @driver.find_element(:css, "#address-body a.overflow-address").click
  wait_for_spinner

  @driver.find_element(:css, "#open-three").click
  wait_for_spinner
end

def apply_coupon
  p "apply coupon"

  datahash = [
    {coupon: "dsada", label: "dsada"},
    {coupon: "cart45", label: "cart45"},
    {coupon: "buy1get2", label: "buy1get2"}
  ]

  datahash.each_with_index do |data, i|
    begin
      @driver.find_element(:css, "input.apply_coupon_code").clear
      @driver.find_element(:css, "input.apply_coupon_code").send_keys data[:coupon]
      @driver.action.send_keys(:enter).perform
      wait_for_spinner
      if @driver.find_element(:css, ".col-808080").text.include? data[:label]
        p "PASS"
      else
        p "FAIL"
      end
    end
  end
  wait_for_spinner
end

def remove_coupon
  if @driver.find_element(:css, ".promo-row").displayed?
    p "removing coupon"
    @driver.find_element(:css, "a.remove_code").click
    wait_for_spinner
  else
    p "unable to remove coupon"
  end
end

def apply_credit
  credits = @driver.find_element(:css, ".order-final-row")
  if credits.text.include? "%credit%"
    p "Applying credits..."
    @driver.find_element(:css, ".credits-show-click a").click
    wait_for_spinner

    @driver.find_element(:css, ".select-credits #sorry").click
    wait_for_spinner
  else
    p "Sorry, you have no credits"
  end
end

def checkout_order
  @driver.find_element(:css, ".proceed-pay-btn").click
  longwait
end

def payment_method_cc
  p "payment_method - CC"
  @driver.find_element(:css, "#credit_card_number").send_keys("5123456789012346")
  wait_for_spinner
  @driver.find_element(:css, "#credit_card_name").send_keys("Test")
  wait_for_spinner
  @driver.find_element(:css, "#credit_card_exp_month").send_keys("05")
  wait_for_spinner
  @driver.find_element(:css, "#credit_card_exp_year").send_keys("2017")
  wait_for_spinner
  @driver.find_element(:css, "#credit_card_ccv").send_keys("123")
  wait_for_spinner
  @driver.find_element(:css, "#creditCardButton").click
  longwait

  p "order comeback"
  thankyou = @driver.find_element(:css, "#thanku_header").text
  wait_for_spinner

  if thankyou.include? "Thank You for Your order"
    p "Thank you for your oder"
  else 
    p "Order was not Successfully placed"
  end
end

def payment_method_cod
  p "payment_method - COD"
  @driver.find_element(:css, "#cod-payment").click
  wait_for_spinner
  codButton = @driver.find_element(:css, "#codButton")
  if codButton.displayed?
    p "Order placing as COD"
    codButton.click
  else
    p "sorry, no COD available"
  end
end

def payment_method_wallet
  p "payment_method - Wallet"
  wait_for_spinner
  @driver.find_element(:css, "#wallet-payment").click
  wait_for_spinner
  wallet = @driver.find_element(:css, "input#walletsButton")
  if wallet.displayed?
    p "Applying wallet ... "
    wallet.click
    wait_for_spinner
    @driver.find_element(:css, "input#walletButton").click
  else
    p "sorry, you are not eligible for applying wallets"
  end
end

def checkoutLogin
  styletag
  login
  # register
  c1
  cart
  continue_login
  # addAddress
  selectAddress
  remove_coupon
  apply_coupon
  apply_credit
  checkout_order
  payment_method_cc
  # payment_method_cod
  # payment_method_wallet
end
checkoutLogin


def checkoutGuest
  styletag
  c1
  cart
  guestLogin
  addAddress
  selectAddress
  apply_coupon
  checkout_order
  # payment_method_cod
  # payment_method_wallet
end
# checkoutGuest