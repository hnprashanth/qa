function waitForSpinner() {
  var EC = protractor.ExpectedConditions;
  // Waits for the element with class 'loading-spiner-holder' to be no longer visible on the dom.
  browser.wait(EC.invisibilityOf($('.loading-spiner-holder')), 5000);
}

describe('ST Checkout Flow', function() {
  it('should login', function() {
    browser.get('http://22.22.22.22:5000/');
    waitForSpinner();
    element(by.css("a.login-link")).click();
    waitForSpinner();
    element(by.model("user.email")).sendKeys('rashmi.un@styletag.com');
  	element(by.model("user.password")).sendKeys('654321');
  	element(by.id("login-btn")).click();
    waitForSpinner();
  });

  it('should click C1', function() {
    browser.get('http://22.22.22.22:5000/');
    waitForSpinner();
    element(by.binding("item.label")).click();
    waitForSpinner();
  });
});