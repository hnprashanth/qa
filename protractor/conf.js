exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['styletag.js'],
  onPrepare: function() {
    browser.manage().window().setSize(1600, 1000);
  }
};