@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @base_url
  @driver.manage.window.maximize  
end

def c1  
  @driver.find_element(:css, "#non-footer > navbar > header > div.grid-container > nav > div > ul > li:nth-child(2) > div > a").click
  sleep(2)
  @driver.find_element(:css, "#sales-container > div > ul > li:nth-child(1) > div.image-wrap > div > a > img").click
  sleep(2)
  @driver.find_element(:css, "#product-container > div.ng-isolate-scope > ul > li:nth-child(2) > div > div.product-image > a > img").click
  begin
    @driver.find_element(:css, ".in-stock:nth-child(1) div").click
    @driver.find_element(:css, "#add-to-cart-button").click
  rescue Exception => e
    @driver.find_element(:css, "#add-to-cart-button").click
    sleep(3)
  end
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
end 

def cart
  @driver.find_element(:css, "#mini-cart-wrap > li > div.minicart-inner-div > a.col-dark-grey.mini-cart-text").click
  sleep(10)
  begin
    cart_text = @driver.find_element(:css, "#cart-container > div > p").text
    p "cart_text: #{cart_text}"
    if cart_text == "Your cart is empty"
      p "Your cart is empty"
    else 
      p "Your cart is not empty"
    end
  rescue Exception => e
    p "Your cart is not empty"
  end
end

def mini_cart
  styletag
  c1
  cart
end
mini_cart
