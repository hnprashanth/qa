@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"
def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @base_url
  @driver.manage.window.maximize
end

def c1
  #c1,c2,product
  sleep(10)
  @driver.find_element(:xpath, "//a[@id='women']").click
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='sales-container']/div/ul[@class='height-auto'] /li[1][@class='bit-sale-3 sale-wrap ng-scope'] /div[@class='sale-text-wrap height-auto']/div[@class='sale-text-content pull-left']/p[@class='sale-name text-left']/a").click
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='product-container']/div[@class='ng-isolate-scope']/ul/li[3]/div[@class='products-wrapper']/div[@class='product-Info']/span[@class='product-description text-left']/a").click
end

def add
  #product view page
  begin
    @driver.find_element(:xpath, "//ol[@id='sizes']/li[1]/div/label").click
    sleep(10)
    @driver.find_element(:xpath, "//div[@id='cart-section']/p[@class='add-cart-wrap']/input").click
    sleep(10)
  rescue Exception => e
    @driver.find_element(:xpath, "//div[@id='cart-section']/p[@class='add-cart-wrap']/input").click
    sleep(10)
  end
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
  # end 
end

def cart
  "cart"
  @driver.find_element(:xpath, "//ul[@id='mini-cart-wrap']/li/div[1]/a[1]").click
  sleep(10)
  qty = @driver.find_element(:xpath, "//div[@id='cart_product_1']/div[4]/label/select/option[2]").click
  sleep(10)
  @driver.find_element(:xpath, "//input[@id='appendedInputButtons']").click
  sleep(10)
  p qty = @driver.find_element(:xpath, "//div[@id='cart_product_1']/div[4]/label/select/option[2]").text.to_i
  sleep(10)
  p price = @driver.find_element(:xpath, "//div[@id='cart_product_1']/div[5]").text.gsub('Rs','').gsub(',','').gsub('.','').gsub('.','').gsub('00','').to_i
  sleep(10)
  p total = qty*price
  sleep(10)
  p actual_total = @driver.find_element(:xpath, "//section[@id='cart-container']/div[1]/div[3]/div[2]/span[2]").text.gsub('Rs','').gsub(',','').gsub(' ','').gsub('.','').gsub('.','').gsub('00','').to_i
end

def all
  styletag
  sleep(10)
  c1
  sleep(10)
  add
  sleep(10)
  cart
  sleep(10)
end
all
