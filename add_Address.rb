require './utils'
include Utils

@dev_url = "http://192.168.1.210:5000/"
@base_url = "http://origin-www.stest.in/"

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @dev_url
  @driver.manage.window.maximize
end

def register
  p "register"
  wait_for_spinner
  @driver.find_element(:css, "a.register-link").click
  wait_for_spinner
  @driver.find_element(:css, "#register_name").clear
  @driver.find_element(:css, "#register_name").send_keys "AmazingTester"
  @driver.find_element(:css, "#register_email").clear
  number = rand(10...9999)
  @driver.find_element(:css, "#register_email").send_keys "test#{number}@example.net"
  @driver.find_element(:css, "#register_password").clear
  @driver.find_element(:css, "#register_password").send_keys "testing123"
  @driver.action.send_keys(:enter).perform
  wait_for_spinner
  unless @driver.find_element(:css, "#account-member-name").displayed?
    raise "User Registration Failed"
  else
    puts "Successfully Registered"
    @driver.save_screenshot("Success-Register.png")
  end
end

def my_account
  sleep(5)
  @driver.find_element(:css, "#account-member-name > em").click	
  wait_for_spinner 
  @driver.find_element(:css, "#account-wrap > div > div:nth-child(1) > div.account-inline.ng-scope > ul > li.show-profile-inner.my-account > a").click  #
  sleep(3)
end

def save_addres
	@driver.find_element(:css, "#Profile-main > p:nth-child(4) > a").click
	wait_for_spinner
	@driver.find_element(:css, "#add-address > button").click
end

def addAddress
  p "add new address"
  wait_for_spinner

  @driver.find_element(:css, "#address_pincode").send_keys("560054")
  wait_for_spinner

  @driver.find_element(:css, "#firstname").send_keys("Test")
  @driver.find_element(:css, "#address-phone").send_keys("9986255327")
  @driver.find_element(:css, "#address_address1").send_keys("#20, Vasanth Nagar, Bangalore")
  @driver.find_element(:css, "#address_landmark").send_keys("Murthy medicals")
  @driver.find_element(:css, "#save-new-address > button.btn.ng-scope").click 
  wait_for_spinner
end

def all
	styletag
	register
    my_account
    save_addres
    addAddress
end
all