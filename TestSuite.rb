@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

require "selenium-webdriver"

@driver = Selenium::WebDriver.for :firefox
@driver.manage.timeouts.implicit_wait = 20

def shortwait
  sleep 5
end

def longwait
  sleep 10
end

def scrolltop
  @driver.find_element(:css, ".col-lg-10 h2").location_once_scrolled_into_view
end

def register
  begin
    @driver.navigate.to @base_url
    @driver.manage.window.maximize
    longwait
    @driver.find_element(:link, "Register").click    
    #datahash for form testing
    datahash = [
      {name: "Amazing Tester", email: "test@prashanth.net", password: "testing123"},
      {name: "AmazingTester", email: "test@prashanth", password: "testing123"},
      {name: "AmazingTester", email: "test@prashanth.net", password: "testing"},
      {name: "", email: "test@prashanth.net", password: "testing123"},
      {name: "AmazingTester", email: "", password: "testing123"},
      {name: "AmazingTester", email: "test@prashanth.net", password: ""}
    ]
    
    datahash.each_with_index do |data, i|
      begin
        @driver.find_element(:css, "#register_name").clear
        @driver.find_element(:css, "#register_name").send_keys data[:name]
        @driver.find_element(:css, "#register_email").clear
        @driver.find_element(:css, "#register_email").send_keys data[:email]
        @driver.find_element(:css, "#register_password").clear
        @driver.find_element(:css, "#register_password").send_keys data[:password]
        if @driver.find_element(:css, ".btn").displayed?
          raise "Register #{i}: Button was supposed to be disabled"
        end
        puts "Register #{i}: Run Succesfully"
      rescue Exception => e 
        @driver.save_screenshot("Register-#{i}.png")
        puts "Register #{i}: "+e.message
        next
      end
    end
    #Checking with pre-existing email id
    @driver.find_element(:css, "#register_name").clear
    @driver.find_element(:css, "#register_name").send_keys "AmazingTester"
    @driver.find_element(:css, "#register_email").clear
    @driver.find_element(:css, "#register_email").send_keys "test@prashanth.net"
    @driver.find_element(:css, "#register_password").clear
    @driver.find_element(:css, "#register_password").send_keys "testing123"
    @driver.action.send_keys(:enter).perform
    shortwait
    unless @driver.find_element(:css, "#st-register").displayed?
      raise "Allowing to register with pre-existing email"
    end
    #Registration with all valid data
    @driver.find_element(:css, "#register_name").clear
    @driver.find_element(:css, "#register_name").send_keys "AmazingTester"
    @driver.find_element(:css, "#register_email").clear
    number = rand(10...9999)
    @driver.find_element(:css, "#register_email").send_keys "test#{number}@example.net"
    @driver.find_element(:css, "#register_password").clear
    @driver.find_element(:css, "#register_password").send_keys "testing123"
    @driver.action.send_keys(:enter).perform
    shortwait
    unless @driver.find_element(:css, "#account-member-name").displayed?
      raise "User Registration Failed"
    else
      puts "Successfully Registered"
      @driver.save_screenshot("Success-Register.png")
    end
  rescue Exception => e 
    @driver.save_screenshot("Test-case.png")
    puts "Test case: "+e.message
  end
  menu = @driver.find_element(:css, "#account-member-name")
  @driver.action.move_to(menu).perform
  @driver.find_element(:css, ".show-profile-inner-logout").click
  sleep(3)
end


def login_positive_scenarios
  p " --login_positive_scenarios-- "
  longwait
  @driver.find_element(:css, "#account-wrap > div > div:nth-child(1) > a").click  
  sleep(3)  
  @driver.find_element(:css, "#login_email").send_keys("example@styletag.com")
  @driver.find_element(:css, "#login_password").send_keys("12345678")
  @driver.find_element(:css, "#login-form > p:nth-child(5) > input").click
  sleep(3)
  loggedin_name = @driver.find_element(:css, "#account-member-name").text
  sleep(2)
  if loggedin_name.include? "Hello"
    p " user logged into styletag: PASS "
  else
    p " user not logged into styletag: FAIL "
  end
end

def logout
  menu = @driver.find_element(:css, "#account-member-name")
  @driver.action.move_to(menu).perform
  @driver.find_element(:css, ".show-profile-inner-logout").click
end

def login_negative_scenarios1 #Form validations
  p " --login_negative_scenarios1-- "
  @driver.find_element(:css, "#account-wrap > div > div:nth-child(1) > a").click  
  sleep(3)
  datahash = [ {email: "example@styletag.com", password: ""},
               {email: "example@styletag.com"},
               {email: "example@styl", password: "12345678"},
               {email: "", password: "12345678"},
               {email: " ", password: "12345678"},
               {email: " ", password: ""} ]

  datahash.each_with_index do |data, i|

    @driver.find_element(:css, "#login_email").clear
    @driver.find_element(:css, "#login_email").send_keys data[:email]
    sleep(3)
    @driver.find_element(:css, "#login_password").clear
    @driver.find_element(:css, "#login_password").send_keys data[:password]
    sleep(3)
    if @driver.find_element(:css, ".btn").displayed?
      p " login_button is enabled - scenarios #{i}: FAIL "
    else 
      p " login_button is disabled: PASS"
    end

    if @driver.find_elements(:css, "#account-member-name").size > 0
      p " user is logged into styletag: FAIL "
    else
      p " user is not logged into styletag: PASS "
    end
  end 
end

def login_negative_scenarios2 #After form submission
  p " --login_negative_scenarios2-- "
  @driver.find_element(:css, "#account-wrap > div > div:nth-child(1) > a").click  
  sleep(3)
  datahash = [ {email: "example@styletag.com", password: "12345"},
               {email: "example@styletag.com", password: " "},
               {email: "example@styletag.com", password: "12345678"} ]

  datahash.each_with_index do |data, i|

    @driver.find_element(:css, "#login_email").clear
    @driver.find_element(:css, "#login_email").send_keys data[:email]
    sleep(3)
    @driver.find_element(:css, "#login_password").clear
    @driver.find_element(:css, "#login_password").send_keys data[:password]
    sleep(3)
    @driver.find_element(:css, "#login-form > p:nth-child(5) > input").click
    sleep(3)
    if @driver.find_elements(:css, "#account-member-name").size > 0
      p " user logged into styletag - scenarios #{i}: FAIL "
    else
      p " user not logged into styletag: PASS "
    end
  end 
end

def login
  login_positive_scenarios
  sleep(3)
  logout
  sleep(3)
  login_negative_scenarios1
  sleep(3)
  login_negative_scenarios2 
end

 register
 login

# @driver.quit
