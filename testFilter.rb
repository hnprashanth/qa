@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.manage.timeouts.implicit_wait = 20
  @driver.navigate.to @dev_url
  @driver.manage.window.maximize
end

def shortwait
  sleep 5
end

def longwait
  sleep 10
end

def c1_sale_filter
  longwait
  p "c1 - sale view"

  @driver.find_element(:css, "a#arts--entertainment").click
  shortwait

  beforeFilterSale = @driver.find_element(:css, "#sale-product-sort > span.total-style-count").text.to_i
  p "#{beforeFilterSale} sale found before filter"
  longwait

  @driver.find_element(:css, ".filter-form #product_type label.custom-label").click
  shortwait

  afterFilterSale = @driver.find_element(:css, "#sale-product-sort > span.col-orange").text.to_i
  p "#{afterFilterSale} sale found after filter"
  shortwait

  actualCountSale = @driver.find_elements(:css, ".arrow-side").size
  p "#{actualCountSale} sale actual count"
  shortwait

  if(afterFilterSale === actualCountSale)
    p "Pass"
  else
    p "Fail"
  end
end

def c1_product_filter
  longwait
  p "c1 - product view"

  @driver.find_element(:css, "a#arts--entertainment").click
  shortwait

  @driver.find_element(:css, "#product_view").click
  longwait

  beforeFilterProd = @driver.find_element(:css, "#sale-product-sort > span.total-style-count").text.to_i
  p "#{beforeFilterProd} products found before filter"
  longwait

  @driver.find_element(:css, ".filter-form #discount label.custom-label").click
  shortwait

  afterFilterProd = @driver.find_element(:css, "#sale-product-sort > span.total-style-count").text.to_i
  p "#{afterFilterProd} products found after filter"
  shortwait

  actualCountProd = @driver.find_elements(:css, ".product-description").size
  p "#{actualCountProd} products actual count"
  shortwait

  if(afterFilterProd === actualCountProd)
    p "Pass"
  else
    p "Fail"
  end
end 

def filter
  styletag
  c1_sale_filter
  c1_product_filter
end
filter