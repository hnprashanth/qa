# Guest-login
@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @base_url
  @driver.manage.window.maximize
end

def c1
  #c1,c2,product
  sleep(10)
  @driver.find_element(:xpath, "//a[@id='women']").click   #c1
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='sales-container']/div/ul[@class='height-auto'] /li[1][@class='bit-sale-3 sale-wrap ng-scope'] /div[@class='sale-text-wrap height-auto']/div[@class='sale-text-content pull-left']/p[@class='sale-name text-left']/a").click  #sale
  sleep(10)
  @driver.find_element(:xpath, "//div[@id='product-container']/div[@class='ng-isolate-scope']/ul/li[3]/div[@class='products-wrapper']/div[@class='product-Info']/span[@class='product-description text-left']/a").click   #product
end


def add
  #product view page
  begin
    @driver.find_element(:xpath, "//ol[@id='sizes']/li[1]/div/label").click  #size
    sleep(10)
    @driver.find_element(:xpath, "//div[@id='cart-section']/p[@class='add-cart-wrap']/input").click  #add_to_cart button
    sleep(10)
  rescue Exception => e
    @driver.find_element(:xpath, "//div[@id='cart-section']/p[@class='add-cart-wrap']/input").click    #mini_cart
    sleep(10)
  end
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
  # end 
end

def cart
  p	"cart"
  @driver.find_element(:xpath, "//ul[@id='mini-cart-wrap']/li/div[1]/a[1]").click     #Proceed to checkout
  sleep(5)
  @driver.find_element(:xpath, "//a[@id='cart-checkout-btn']").click
  sleep(5)
end

def guest_login
  #guest_login
  @driver.find_element(:xpath, "//div[@id='auth-body']/div/div[2]/form/p[1]/input").send_keys("abc@styletag.com")  #email_id field
  sleep(5)
  @driver.find_element(:xpath, "//div[@id='auth-body']/div/div[2]/form/p[3]/input").click    # radio button (Continue as guest)
  sleep(5)
  @driver.find_element(:xpath, "//div[@id='auth-body']/div/div[2]/form/p[4]/button").click  #Continue as guest button
  sleep(5)
  # @driver.find_element(:css, "#open-two").click
end

def login_verify
  p "verifying user is loggedin or not"
  sleep(10)
  p loggedin_name = @driver.find_element(:xpath, "//a[@id='account-member-name']/em").text  #account-member-name
  sleep(10)
  if [loggedin_name].include? "Hello, Guest"
    p "User is logged into styletag: PASS"
  else
    p " User is not logged into styletag: FAIL "
  end
end

def all
  styletag
  c1
  add
  cart
  guest_login
  login_verify
end
all

