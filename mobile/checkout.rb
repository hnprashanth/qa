require './utils'
include Utils

#checkout
@dev_url = "http://22.22.22.22:5500/"
@base_url = "http:m.stest.in/"

def shortwait
  sleep 5
end

def longwait
  sleep 10
end

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @driver.navigate.to @dev_url
  @driver.manage.window.maximize
  # browser = Capybara.current_session.driver.browser
  # browser.manage.delete_all_cookies
  # # @driver.manage.deleteAllCookies
end 

def login
  p "login"
  wait_for_mobile_spinner
  @driver.find_element(:css, ".sidebar-toggle").click
  shortwait
  wait_for_mobile_spinner
  @driver.find_element(:css, "#st-login").click  
  wait_for_mobile_spinner 
  @driver.find_element(:css, "#login_email").send_keys("ram@gmail.com")
  @driver.find_element(:css, "#login_password").send_keys("ramramram")
  @driver.find_element(:css, "#login-btn").click
end

def c1
  wait_for_mobile_spinner
  p "c1,sale,product"
  wait_for_mobile_spinner
  @driver.find_element(:css, ".sidebar-toggle").click
  shortwait
  wait_for_mobile_spinner
  @driver.find_element(:css, "a.arts--entertainment").click
  shortwait
  wait_for_mobile_spinner
  @driver.find_element(:css, "a.list-item-grandchild.fanny-packs").click
  shortwait
  wait_for_mobile_spinner
  @driver.find_element(:css, "a.list-item-grandchild.Sales").click
  wait_for_mobile_spinner
  @driver.find_element(:css, ".sale-category-wrap div a").click
  wait_for_mobile_spinner
  @driver.find_element(:css, ".products-inner a.image-border").click                 
  begin
    wait_for_mobile_spinner
    @driver.find_element(:css, ".in-stock:nth-child(1) div").click
    @driver.find_element(:css, ".add-cart").click
  rescue Exception => e
    @driver.find_element(:css, ".add-cart").click
    wait_for_mobile_spinner
  end
  # rescue Selenium::WebDriver::Error::NoSuchElementError
  # p "Product got soldput: Test case failed at Product view page"
end 

def cart
  p "cart"
  @driver.find_element(:css, ".btn-group.pull-right btn").click
  shortwait
  wait_for_mobile_spinner
  a = @driver.find_element(:css, "#cart-wrap > .cart-inner-repeat .cart-info-variant").text
  wait_for_mobile_spinner
  if a.include? ("size")
    p "Product is added to cart: PASS"
  else
    p "Product not added to cart: FAIL"
  end
  wait_for_mobile_spinner
  @driver.find_element(:css, "#cart-checkout-btn").click
  wait_for_mobile_spinner
end

def guestLogin
  p "guest login"  
  @driver.find_element(:css, "#checkout-login_email").send_keys("testexp123@styletag.com")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#checkout-login_false").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#continue-guest-btn").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#open-two").click
  wait_for_mobile_spinner
end

def addAddress
  p "add new address"
  # click add new address
  @driver.find_element(:css, ".add-address-block > a#add-address-checkout").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#address_pincode").send_keys("560054")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#firstname").send_keys("Test")
  @driver.find_element(:css, "#address-phone").send_keys("9986255327")
  @driver.find_element(:css, "#address_address1").send_keys("#20, Vasanth Nagar, Bangalore")
  @driver.find_element(:css, "#address_landmark").send_keys("Murthy medicals")
  @driver.find_element(:css, "#create-btn").click
  # # click to order summary
  wait_for_mobile_spinner
  # @driver.find_element(:css, "#address-body a.overflow-address").click
  # wait_for_mobile_spinner
  @driver.find_element(:css, "#open-three").click
  wait_for_mobile_spinner
  # puts "-" * 10
  # button = @driver.find_element(:css, ".proceed-pay-btn")
  # p button
  # button.click
  # wait_for_mobile_spinner
  # p "proceed to pay"
end

def selectAddress
  @driver.find_element(:css, "#address-body a.overflow-address").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#open-three").click
  wait_for_mobile_spinner
end

def apply_coupon
  @driver.find_element(:css, "input.apply_coupon_code").send_keys("dsada")
  wait_for_mobile_spinner
  @driver.find_element(:css, "input.apply_coupon_code").clear
  wait_for_mobile_spinner
  @driver.find_element(:css, "input.apply_coupon_code").send_keys("happy20")
  wait_for_mobile_spinner
  if @driver.find_element(:css, ".col-808080").text.include?("%happy20%")
    p "PASS"
  else
    p "FAIL"
  end
  wait_for_mobile_spinner
  @driver.find_element(:css, ".apply-coupon-btn").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "input.apply_coupon_code").clear
  wait_for_mobile_spinner
  @driver.find_element(:css, "input.apply_coupon_code").send_keys("product30")
  wait_for_mobile_spinner
  if @driver.find_element(:css, ".col-808080").text.include?("%product30%")
    p "PASS"
  else
    p "FAIL"
  end
  wait_for_mobile_spinner
  @driver.find_element(:css, ".apply-coupon-btn").click
  wait_for_mobile_spinner
end

def apply_credit
  @driver.find_element(:css, "#credit.name").click
end

def checout_order
  @driver.find_element(:css, ".proceed-pay-btn").click
  wait_for_mobile_spinner
end

def payment_method_cc
  p "payment_method - CC"
  @driver.find_element(:css, "#cc-payment").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#credit_card_number").send_keys("5123456789012346")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#credit_card_name").send_keys("Test")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#credit_card_exp_month").send_keys("05")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#credit_card_exp_year").send_keys("2017")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#credit_card_ccv").send_keys("123")
  wait_for_mobile_spinner
  @driver.find_element(:css, "#creditCardButton").click
end

def payment_method_cod
  p "payment_method - COD"
  @driver.find_element(:css, "#cod-payment").click
  wait_for_mobile_spinner
  @driver.find_element(:css, "#codButton").click
end

def payment_method_wallet
  p "payment_method - Wallet"
  @driver.find_element(:css, "#wallet-payment").click
end

def checkoutLogin
  styletag
  login
  c1
  cart
  # addAddress
  # checout_order
  # payment_method_cod
end
checkoutLogin


def checkoutGuest
  styletag
  c1
  cart
  guestLogin
  addAddress
  selectAddress
  checout_order
  payment_method_cod
end
# checkoutGuest