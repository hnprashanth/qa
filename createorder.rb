@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

require "selenium-webdriver"

@driver = Selenium::WebDriver.for :firefox

def shortwait
  sleep 5
end

def longwait
  sleep 10
end

def scrolltop
  @driver.find_element(:css, ".col-lg-10 h2").location_once_scrolled_into_view
end

def datahash
  array_of_hashes = [
    {email: "a@styletag.com", label: "leggings", size: "M", quantity: "1", pincode: "560052", firstname: "Testing", phone: "9845098450", address: "No 88, Blah Street, BlahBlah Nagar"}, 
    {email: "rashmi.un@styletag.com", label: "kurta", size: "L", quantity: "1", pincode: "560052", firstname: "Testing 2", phone: "1234567890", address: "No 44, Blah Street, BlahBlah Nagar"}
  ]
  return array_of_hashes
end

datahash.each_with_index do |data, i|
  begin
    @driver.navigate.to @base_url
    @driver.manage.window.maximize
    shortwait
    @driver.find_element(:link, "CS Dashboard").click
    @driver.find_element(:link, "Create Order").click
    shortwait
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:name, "search")).select_by(:text, "Email")
    @driver.find_element(:css, "input[name=\"search\"]").clear
    @driver.find_element(:css, "input[name=\"search\"]").send_keys data[:email]
    scrolltop
    @driver.find_element(:xpath, "//button[@type='submit']").click
    shortwait
    @driver.find_element(:css, "button.btn.btn-default").click
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[3]/div[2]/form/div/select")).select_by(:text, "sku")
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[3]/div[2]/form/div/select")).select_by(:text, "label")
    @driver.find_element(:name, "search").clear
    @driver.find_element(:name, "search").send_keys data[:label]
    scrolltop
    @driver.find_element(:xpath, "//button[@type='submit']").click
    shortwait
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[4]/table/tbody/tr/td[2]/div/form/div/select")).select_by(:text, data[:size])
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[4]/table/tbody/tr/td[2]/div/form/div[2]/select")).select_by(:text, data[:quantity])
    scrolltop
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[4]/table/tbody/tr/td[2]/div/form/div[3]/button").click
    shortwait
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/div/button[2]").click
    shortwait
    @driver.find_element(:css, ".pull-right").click
    @driver.find_element(:name, "pincode").clear
    @driver.find_element(:name, "pincode").send_keys data[:pincode]
    @driver.find_element(:name, "firstname").clear
    @driver.find_element(:name, "firstname").send_keys data[:firstname]
    @driver.find_element(:name, "phone").clear
    @driver.find_element(:name, "phone").send_keys data[:phone]
    @driver.find_element(:xpath, "//input[@type='checkbox']").click
    @driver.find_element(:name, "address").clear
    @driver.find_element(:name, "address").send_keys data[:address]
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[3]/div/div/form/p[9]/button").click
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:name, "bcountry")).select_by(:text, "India")
    Selenium::WebDriver::Support::Select.new(@driver.find_element(:name, "country")).select_by(:text, "India")
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[3]/div/div/form/p[9]/button").click
    shortwait
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[3]/div/div[2]/form/p[9]/button").click
    shortwait
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/fieldset/div[4]/button[2]").click
    shortwait
    @driver.find_element(:xpath, "//div[@id='order-create']/div/div/div/div[2]/div/button[2]").click
    shortwait
    @driver.find_element(:css, "button.btn.btn-default").click
    shortwait
    @driver.find_element(:css, "div.pull-right > button.btn.btn-success").click
    puts "Test case #{i}: Ran Successfully"
  rescue Exception => e 
    @driver.save_screenshot("Test-case-#{i}.png")
    puts "Test case #{i}: "+e.message
    next
  end
end
@driver.quit