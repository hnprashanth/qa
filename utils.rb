module Utils

  def shortwait
    sleep 5
  end

  def longwait
    sleep 10
  end

  def wait_for_spinner
    spinner = @driver.find_element(:css, ".loading-spiner-holder")
    while spinner.displayed?
      sleep  2
      p "Spinner"
    end
  end

  def wait_for_mobile_spinner
    loading_spinner = @driver.find_element(:css, ".app-content-loading")
    while loading_spinner.displayed?
      sleep  2
      p "Spinner"
    end
  end

end
