@dev_url = "http://22.22.22.22:5000/"
@base_url = "http://origin-www.stest.in/"

def styletag
  require "selenium-webdriver"
  @driver = Selenium::WebDriver.for :firefox
  @base_url = "http://192.168.1.12/#/"
  @driver.navigate.to @base_url
  @driver.manage.window.maximize  
end

def search
  @driver.find_element(:css, "#appendedInputButtons").send_keys("saree")
  @driver.find_element(:css, "#submitButton").click
  sleep(10)
end

def order
  # @driver.find_element(:css, "#price-htl-sort > a:nth-child(2)").click   #Low To High
  @driver.find_element(:css, "#price-htl-sort > a:nth-child(4)").click  #High To Low
  sleep(10)
end

def price_compare
  prices = []
  for i in 1..10
    prices << @driver.find_element(:css, "#product-container > div > ul > li:nth-child(#{i}) > div > div.product-Info > span.product-price.pull-right > span.col-orange.text-right > a").text.gsub('Rs.','').gsub(' ','').gsub(',','').to_i
    @driver.execute_script("window.scrollBy(0,500)", "")
    @driver.execute_script("window.scrollBy(0,100)", "")
    @driver.execute_script("window.scrollBy(100,-90)", "")
    sleep(2)
  end
  p prices
  p pc = prices.count

  # for i in 0..pc
  #   begin
  #     if prices[i] <= prices[i+1]
  #       p "In ascending order: Pass"
  #     else
  #       p "Not in ascending order: Fail"
  #       @driver.save_screenshot("Test-case.png")
  #     end
  #   rescue Exception => e
  #     p prices[i]      
  #   end
  # end

  for i in 0..pc
    begin
      if prices[i] >= prices[i+1]
        p "In desending order: Pass"
      else
        p "Not in desending order: Fail"
        @driver.save_screenshot("Test-case.png")
      end
    rescue Exception => e
      p prices[i]      
    end
  end

end

def search_price_compare
  styletag
  search
  order
  price_compare
end
search_price_compare
